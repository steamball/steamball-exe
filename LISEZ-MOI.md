SteamBall Exe
=============

L'exécutable de SteamBall pour win64 pour un casque de réalité virtuelle (testé
seulement sur Occulus Rift S).

*An english verions of the explanations is avaible in*
[README.md](https://gitlab.com/steamball/steamball-exe/-/blob/master/README.md)
*.*

* Sommaire :
    * [Introduction](#introduction)
    * [Vidéo de démonstration](#vidéo-de-démonstration)
    * [Règles du jeu](#règles-du-jeu-)
    * [Contrôles](#contrôles)
    * [Installation pour un casque VR avec des contrôleurs VR](#installation-pour-un-casque-vr-avec-des-contrôleurs-vr)
        * [Matériel nécessaire](#matériel-nécessaire-)
        * [Instructions](#instructions-)
    * [Autres installations pour un casque VR avec deux Joy-Cons](#autres-installations-pour-un-casque-vr-avec-deux-joy-cons)

Introduction
============

Vidéo de démonstration
----------------------

Une vidéo de démonstration est disponible sur [YouTube](https://www.youtube.com/watch?v=ds3Cbeal-sQ).  
[![youtube](https://img.youtube.com/vi/ds3Cbeal-sQ/0.jpg)](https://www.youtube.com/watch?v=ds3Cbeal-sQ).

Règles du jeu :
---------------

SteamBall est un flipper 3D en réalité virtuelle. Vous contrôlez deux palets et
vous devez taper les balles pour les conserver dans la zone de jeu. Faites-les
rebondir sur les objets pour gagner des points. Des quêtes spéciales peuvent
vous faire remporter des récompenses spéciales. Si vous perdez toutes les
balles, la partie est finie. Faites exploser le compteur de score !

* Scores :
    * Cibles permanentes :
        * Bumper : 100 points
        * Voiture : 200 points
        * Lampadaire : 400 points
    * Cibles destructibles :
        * Borne incendie : 2 000 points
        * Pylône électrique : 5 000 points
        * Pancarte de magasin : 2 000 points
        * Tonneau (Un tonneau apparait toutes les 10s) : 500 points
        * Engrenage aérien : 3 000 points
        * Montgolfière : 200 points per hit
* Quêtes :
    * Détruire les 6 bornes incendie : 25 000 points + 1 balle + 2 :30 min de stopper
    * Détruire les 6 pylônes électrique : 50 000 points + 1 balle + Nouvelle quête Montgolfière
    * Détruire la montgolfière (doit être touchée 50 fois) : 100 000 points + 3 balles
    * Toucher toutes les pancartes de magasin : 40 000 points + 1 balle + diminution du temps d'apparition des tonneaux à 5s.
    * Détruire 10 tonneaux : 10 000 points + 1 balle
    * Toucher les 5 engrenages aérien : 40 000 points + 1 balle

Contrôles
---------

-   Contrôles principaux :
    -   Dans le menu, appuyer sur n'importe lequel de ces boutons (←, ↑, →, ↓,
        A, B, X, Y) pour lancer la partie.
    -   Appuyer sur F4 or Alt + F4 pour quitter le jeu
-   Autres contrôles :
    -   Appuyer sur Ctrl + Maj + V pour lancer le mode pour casque VR (mode par
        défaut)
    -   Appuyer sur Ctrl + Maj + X pour lancer le mode simulation de CAVE
        (expérimentale, vous aurez besoin d'un ordinateur très puissant pour
        pouvoir jouer sans avoir de nausées)
    -   Appuyer sur Ctrl + Maj + C pour avoir la sortie d'écran pour CAVE
    -   Appuyer sur Ctrl + Maj + D pour avoir la sortie d'écran pour DOME (vous
        aurez besoin d'un DOME avec une entrée compatible fish-eye caméra 180°)

Installation pour un casque VR avec des contrôleurs VR
======================================================

Matériel nécessaire :
---------------------

-   Un casque de réalité virtuelle compatible avec SteamVR
-   Un ordinateur Windows 10 64bit compatible VR et une carte graphique ayant au
    moins 2048MB de mémoire vidéo dédiée.
-   Deux contrôleurs VR ou deux Joy-Cons et une connexion Bluetooth sur
    l'ordinateur.
-   Steam VR installé.

Instructions :
--------------

1.  Télécharger executable.zip à partir de ce repository or de ce
    [lien](https://ludusacademie-my.sharepoint.com/:u:/g/personal/b_fache_ludus-academie_com/EeDx5nH_ZRJKkZpuN2UNEa4BUUOAANQX9E5K_Roo4UOpKw?e=d1aYCW).
2.  Installer SteamVR et configurer votre casque avec SteamVR. (Instructions
    [here](https://www.youtube.com/watch?v=TxwhBhZGwfU))
3.  Connecter votre casque VR
4.  Décompressez l'archive executable.zip
5.  Exécuter SteamBall.exe dans "exeV4/WindowsNoEditor" pour lancer le jeu.
6.  Dans le menu, appuyer sur l'un de ces boutons (←, ↑, →, ↓, A, B, X, Y) de
    vos contrôleurs pour lancer la partie.
7.  Pour quitter le jeu appuyer sur F4 ou sur Alt + F4

Autres installations pour un casque VR avec deux Joy-Cons
=========================================================

*Les instructions 1. 2. 3. 4. sont les mêmes.*

1.  Avant de lancer le jeu, visitez <https://gitlab.com/steamball/BetterJoy> et
    suivez the instructions.
2.  Exécuter SteamBall.exe dans "exeV4/WindowsNoEditor" pour lancer le jeu.
3.  Dans le menu, appuyer sur l'un de ces boutons (←, ↑, →, ↓, A, B, X, Y) de
    vos contrôleurs pour lancer la partie.
4.  Pour quitter le jeu appuyer sur F4 ou sur Alt + F4

*Si vous voulez essayer un autre type de configuration comme un DOME ou une
CAVE, contactez-nous (les Joy-Cons sont nécessaires pour ces configurations).*
