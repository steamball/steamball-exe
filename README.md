# SteamBall Exe
The executable of SteamBall for win64 for VR Headset (only tested on Occulus Rift S).

*Une version française des explications est disponible dans le fichier [LISEZ-MOI.md](https://gitlab.com/steamball/steamball-exe/-/blob/master/LISEZ-MOI.md) .*

* Summary:
    * [Introduction](#introduction)
    * [Gameplay demo video](#gameplay-demo-video)
    * [How To Play](#how-to-play)
    * [Controls](#controls)
    * [Main installation for VR headset with VR motion controllers](#main-installation-for-vr-headset-with-vr-motion-controllers)
        * [Requirements](#requirements)
        * [Instructions](#instructions)
    * [Other installations for VR headset with two Joy-Con](#other-installations-for-VR-headset-with-two-Joy-Con)

# Introduction

## Gameplay demo video
A gameplay demonstration video available in french on [youtube](https://www.youtube.com/watch?v=ds3Cbeal-sQ).  
[![youtube](https://img.youtube.com/vi/ds3Cbeal-sQ/0.jpg)](https://www.youtube.com/watch?v=ds3Cbeal-sQ).

## How To Play:
This game is a 3D flipper game in virtual reality. You controls two planks and you have to hit the balls in order to maintain them in the level. 
Make them bounce on structure as much as yu can to earn points. There are special quests that will make you earn jackpots.
If you lose all the balls, the game is over. You have to try to beat your highscore.

* Scores :
    * Permanent targets :
        * Bumper : 100 points
        * Car : 200 points
        * Street light : 400 points
    * Destructible targets :
        * Hydrant : 2 000 points
        * Electric pylon : 5 000 points
        * Shop sign : 2 000 points
        * Barrel (one barrel spawn every 10s) : 500 points
        * Aerial gear : 3 000 points
        * Hot-air balloon : 200 points per hit
* Quests :
    * Destroy the 6 hydrant : 25 000 points + 1 ball + 2:30 min of stopper
    * Destroy the 6 electric pylon : 50 000 points + 1 ball + New quest hot-air balloon
    * Destroy the hot-air balloon (need to be hit 50 times) : 100 000 points + 3 balls
    * Hit all the shop signs : 40 000 points + 1 ball + decrease of the barrels spawning time  to 5s
    * Destroy 10 barrels : 10 000 points + 1 ball
    * Hit the 5 aerial gears : 40 000 points + 1 ball


## Controls
* Main controls :
    * In the main menu, press any of this button to start the game (←, ↑, →, ↓, A, B, X, Y)
    * Press F4 or Alt + F4 to quit the game
* Other controls:
    * Press Ctrl + Shift + V to launch the VR headset mode (default mode)
    * Press Ctrl + Shift + X to launch the CAVE simulation mode (experimentale, you will need a very powerfull computer to be able to play without having a lot of motion sickness)
    * Press Ctrl + Shift + C to launch the CAVE screen output (you need a CAVE to be able to play with this mode)
    * Press Ctrl + Shift + D to launch the DOME screen output (you need a DOME with a 180° fisheye camera entry to play with it)

# Main installation for VR headset with VR motion controllers

## Requirements:

* A virtual reality headset compatible with SteamVR
* A windows 10 64bit computer vr ready with a graphic card with at least 2048 MB dediacated video ram.
* Two virtual reality motion controllers or two Joy-Cons and a bluetooth connection on your computer.
* Steam VR installed

## Instructions:
1. Download executable.zip from this repository or from this [link](https://ludusacademie-my.sharepoint.com/:u:/g/personal/b_fache_ludus-academie_com/EeDx5nH_ZRJKkZpuN2UNEa4BUUOAANQX9E5K_Roo4UOpKw?e=d1aYCW).
2. Install SteamVR and configure your headset with steamVR. (Instructions [here](https://www.youtube.com/watch?v=TxwhBhZGwfU))
3. Connect your VR headset
4. Unzip the downloaded folder
5. Run SteamBall.exe in "exeV4/WindowsNoEditor" to launch the game.
6. In the menu, press any button (←, ↑, →, ↓, A, B, X, Y) of your controllers to start the game.
7. To quit the game press F4 or Alt + F4
    

# Other installations for VR headset with two Joy-Con
*__1. 2. 3. 4.__ instructions are the same.*
5. Before running the game, go to [https://gitlab.com/steamball/BetterJoy](https://gitlab.com/steamball/BetterJoy) and follow the instructions.
6. Run SteamBall.exe in "exeV4/WindowsNoEditor" to launch the game.
7. In the menu, press any button (←, ↑, →, ↓, A, B, X, Y) of your controllers to start the game.
8. To quit the game press F4 or Alt + F4

*If you want to try any other type of setup as for a DOME or a CAVE, please contact us (Joy-Cons are needed)*
    